#!/bin/bash
####################################################
#                                                  #
# This is a L2TP VPN installation for CentOS 7     #
# Version: 1.1.0 20140803                          #
# Author: Travis Lee                               #
# Website: http://www.stunnel.info                 #
#                                                  #
####################################################
# modified script to work with CentminMod.com LEMP
# https://github.com/travislee8964/L2TP-VPN-installation-script-for-CentOS-7/blob/master/l2tp-ipsec-install-script-for-centos7.sh
# https://github.com/xelerance/Openswan/issues/85

if [ -f /proc/user_beancounters ]; then
    echo
    echo "OpenVZ Virtualization Detected"
    echo "This installer is does not support OpenVZ"
    echo "Xen, KVM or dedicated servers only"
    echo
    exit
fi

if [[ $(id -u) != "0" ]]; then
    printf "\e[42m\e[31mError: You must be root to run this install script.\e[0m\n"
    exit 1
fi

if [[ $(grep "release 7." /etc/redhat-release 2>/dev/null | wc -l) -eq 0 ]]; then
    printf "\e[42m\e[31mError: Your OS is NOT CentOS 7 or RHEL 7.\e[0m\n"
    printf "\e[42m\e[31mThis install script is ONLY for CentOS 7 and RHEL 7.\e[0m\n"
    exit 1
fi
clear

printf "
####################################################
#                                                  #
# This is a L2TP VPN installation for CentOS 7     #
# Version: 1.1.0 20140803                          #
# Author: Travis Lee                               #
# Website: http://www.stunnel.info                 #
#                                                  #
####################################################
"

serverip=$(hostname -i)
printf "\e[33m$serverip\e[0m is the server IP?"
printf "If \e[33m$serverip\e[0m is \e[33mcorrect\e[0m, press enter directly."
printf "If \e[33m$serverip\e[0m is \e[33mincorrect\e[0m, please input your server IP."
printf "(Default server IP: \e[33m$serverip\e[0m):"
read serveriptmp
if [[ -n "$serveriptmp" ]]; then
    serverip=$serveriptmp
fi

ethlist=$(ifconfig | grep ": flags" | cut -d ":" -f1)
eth=$(printf "$ethlist\n" | head -n 1)
if [[ $(printf "$ethlist\n" | wc -l) -gt 2 ]]; then
    echo ======================================
    echo "Network Interface list:"
    printf "\e[33m$ethlist\e[0m\n"
    echo ======================================
    echo "Which network interface you want to listen for ocserv?"
    printf "Default network interface is \e[33m$eth\e[0m, let it blank to use default network interface: "
    read ethtmp
    if [ -n "$ethtmp" ]; then
        eth=$ethtmp
    fi
fi

iprange="10.0.1"
echo "Please input IP-Range:"
printf "(Default IP-Range: \e[33m$iprange\e[0m): "
read iprangetmp
if [[ -n "$iprangetmp" ]]; then
    iprange=$iprangetmp
fi

salt=$(echo $RANDOM$RANDOM)

mypsk="${salt}.info"
echo "Please input PSK:"
printf "(Default PSK: \e[33m${salt}.info\e[0m): "
read mypsktmp
if [[ -n "$mypsktmp" ]]; then
    mypsk=$mypsktmp
fi

username="${salt}"
echo "Please input VPN username (leave empty & hit enter for multi user setup) : "
printf "(Default VPN username: \e[33m${username}\e[0m): "
read usernametmp
if [[ -n "$usernametmp" ]]; then
    username=${usernametmp}
elif [[ -z "$usernametmp" ]]; then
    MULTIUSER=y
    username=${username}
    usernameb=${username}b
    usernamec=${username}c
    usernamed=${username}d
    usernamee=${username}e
    usernamef=${username}f
    usernameg=${username}g
    usernameh=${username}h
fi

randstr() {
    index=0
    str=""
    for i in {a..z}; do arr[index]=$i; index=$(expr ${index} + 1); done
    for i in {A..Z}; do arr[index]=$i; index=$(expr ${index} + 1); done
    for i in {0..9}; do arr[index]=$i; index=$(expr ${index} + 1); done
    for i in {1..10}; do str="$str${arr[$RANDOM%$index]}"; done
    echo $str
}

password=$(randstr)
printf "Please input \e[33m$username\e[0m's password (leave empty & hit enter for multi user setup) :\n"
printf "Default password is \e[33m$password\e[0m, let it blank to use default password: "
read passwordtmp
if [[ -n "$passwordtmp" ]]; then
    password=${passwordtmp}
elif [[ -z "$passwordtmp" ]]; then
    MULTIUSERPASS=y
    password=${password}
    passwordb=${password}b
    passwordc=${password}c
    passwordd=${password}d
    passworde=${password}e
    passwordf=${password}f
    passwordg=${password}g
    passwordh=${password}h
fi

clear

clear
echo "Server IP:"
echo "$serverip"
echo
echo "Server Local IP:"
echo "$iprange.1"
echo
echo "Client Remote IP Range:"
echo "$iprange.10-$iprange.254"
echo
echo "PSK:"
echo "$mypsk"
echo
echo "Press any key to start..."

get_char() {
    SAVEDSTTY=`stty -g`
    stty -echo
    stty cbreak
    dd if=/dev/tty bs=1 count=1 2> /dev/null
    stty -raw
    stty echo
    stty $SAVEDSTTY
}
char=$(get_char)
clear
mknod /dev/random c 1 9

yum -y update
yum install -y openswan ppp xl2tpd wget

rm -f /etc/ipsec.conf

cat >>/etc/ipsec.conf<<EOF
# /etc/ipsec.conf - Libreswan IPsec configuration file

# This file:  /etc/ipsec.conf
#
# Enable when using this configuration file with openswan instead of libreswan
#version 2
#
# Manual:     ipsec.conf.5

# basic configuration
config setup
    # NAT-TRAVERSAL support, see README.NAT-Traversal
    nat_traversal=yes
    # exclude networks used on server side by adding %v4:!a.b.c.0/24
    virtual_private=%v4:10.0.0.0/8,%v4:192.168.0.0/16,%v4:172.16.0.0/12
    # OE is now off by default. Uncomment and change to on, to enable.
    oe=off
    # which IPsec stack to use. auto will try netkey, then klips then mast
    protostack=netkey
    #force_keepalive=yes
    keep_alive=60
    # enable logging set plutodebug to “control”. This will log messages to /var/log/pluto.log
    # plutodebug=none

conn L2TP-PSK-NAT
    rightsubnet=vhost:%priv
    also=L2TP-PSK-noNAT
    nat-keepalive=yes

conn L2TP-PSK-noNAT
    authby=secret
    pfs=no
    auto=add
    keyingtries=3
    rekey=no
    ikelifetime=8h
    keylife=1h
    type=transport
    left=$serverip
    leftid=$serverip
    leftprotoport=17/1701
    right=%any
    rightprotoport=17/%any
    # Dead Peer Dectection (RFC 3706) keepalives delay
    dpddelay=40
    #  length of time (in seconds) we will idle without hearing either an R_U_THERE poll from our peer, or an R_U_THERE_ACK reply.
    dpdtimeout=130
    # When a DPD enabled peer is declared dead, what action should be taken. clear means the eroute and SA with both be cleared
    dpdaction=clear
    leftnexthop=$serverip
    rightnexthop=%defaultroute
# For example connections, see your distribution's documentation directory,
# or the documentation which could be located at
#  /usr/share/docs/libreswan-3.*/ or look at https://www.libreswan.org/
#
# There is also a lot of information in the manual page, "man ipsec.conf"

# You may put your configuration (.conf) file in the "/etc/ipsec.d/" directory
# by uncommenting this line
#include /etc/ipsec.d/*.conf
EOF

rm -f /etc/ipsec.secrets
cat >>/etc/ipsec.secrets<<EOF
#include /etc/ipsec.d/*.secrets
$serverip %any: PSK "$mypsk"
EOF

mkdir -p /etc/xl2tpd
rm -f /etc/xl2tpd/xl2tpd.conf
cat >>/etc/xl2tpd/xl2tpd.conf<<EOF
;
; This is a minimal sample xl2tpd configuration file for use
; with L2TP over IPsec.
;
; The idea is to provide an L2TP daemon to which remote Windows L2TP/IPsec
; clients connect. In this example, the internal (protected) network
; is 192.168.1.0/24.  A special IP range within this network is reserved
; for the remote clients: 192.168.1.128/25
; (i.e. 192.168.1.128 ... 192.168.1.254)
;
; The listen-addr parameter can be used if you want to bind the L2TP daemon
; to a specific IP address instead of to all interfaces. For instance,
; you could bind it to the interface of the internal LAN (e.g. 192.168.1.98
; in the example below). Yet another IP address (local ip, e.g. 192.168.1.99)
; will be used by xl2tpd as its address on pppX interfaces.
[global]
; ipsec saref = yes
listen-addr = $serverip
auth file = /etc/ppp/chap-secrets
port = 1701
[lns default]
ip range = $iprange.10-$iprange.254
local ip = $iprange.1
refuse chap = yes
refuse pap = yes
require authentication = yes
name = L2TPVPN
ppp debug = yes
pppoptfile = /etc/ppp/options.xl2tpd
length bit = yes
EOF

mkdir -p /etc/ppp
rm -f /etc/ppp/options.xl2tpd
cat >>/etc/ppp/options.xl2tpd<<EOF
#require-pap
#require-chap
#require-mschap
ipcp-accept-local
ipcp-accept-remote
require-mschap-v2
ms-dns 8.8.8.8
ms-dns 4.2.2.2
ms-dns 8.8.4.4
asyncmap 0
auth
crtscts
lock
hide-password
modem
debug
name l2tpd
proxyarp
lcp-echo-interval 30
lcp-echo-failure 4
mtu 1300
mru 1300
noccp
connect-delay 5000
# To allow authentication against a Windows domain EXAMPLE, and require the
# user to be in a group "VPN Users". Requires the samba-winbind package
# require-mschap-v2
# plugin winbind.so
# ntlm_auth-helper '/usr/bin/ntlm_auth --helper-protocol=ntlm-server-1 --require-membership-of="EXAMPLE\VPN Users"'
# You need to join the domain on the server, for example using samba:
# http://rootmanager.com/ubuntu-ipsec-l2tp-windows-domain-auth/setting-up-openswan-xl2tpd-with-native-windows-clients-lucid.html
EOF

rm -f /etc/ppp/chap-secrets

if [[ "$MULTIUSER" = y && "$MULTIUSERPASS" = y ]]; then

cat >>/etc/ppp/chap-secrets<<EOF
# Secrets for authentication using CHAP
# client     server     secret               IP addresses
$username          l2tpd     $password               *
$usernameb         l2tpd     $passwordb               *
$usernamec         l2tpd     $passwordc               *
$usernamed         l2tpd     $passwordd               *
$usernamee         l2tpd     $passworde               *
$usernamef         l2tpd     $passwordf               *
$usernameg         l2tpd     $passwordg               *
$usernameh         l2tpd     $passwordh               *
EOF

else

cat >>/etc/ppp/chap-secrets<<EOF
# Secrets for authentication using CHAP
# client     server     secret               IP addresses
$username          l2tpd     $password               *
EOF

fi

chmod 0640 /etc/ppp/chap-secrets

sysctl -w net.ipv4.ip_forward=1
sysctl -w net.ipv4.conf.all.rp_filter=0
sysctl -w net.ipv4.conf.default.rp_filter=0
sysctl -w net.ipv4.conf.$eth.rp_filter=0
sysctl -w net.ipv4.conf.all.send_redirects=0
sysctl -w net.ipv4.conf.default.send_redirects=0
sysctl -w net.ipv4.conf.all.accept_redirects=0
sysctl -w net.ipv4.conf.default.accept_redirects=0

# corrections for centmin mod lemp stacks
# https://github.com/centminmod/centminmod/blob/123.08stable/inc/nginx_install.inc#L144-L185

sed -i 's|net.ipv4.conf.all.rp_filter = 1|net.ipv4.conf.all.rp_filter = 0|g' /etc/sysctl.conf
sed -i 's|net.ipv4.conf.default.rp_filter = 1|net.ipv4.conf.default.rp_filter = 0|g' /etc/sysctl.conf

cat >>/etc/sysctl.conf<<EOF

# comment out ones already set by centminmod installer
net.ipv4.ip_forward = 1
# net.ipv4.conf.all.rp_filter = 0
# net.ipv4.conf.default.rp_filter = 0
net.ipv4.conf.$eth.rp_filter = 0
# net.ipv4.conf.all.send_redirects = 0
# net.ipv4.conf.default.send_redirects = 0
# net.ipv4.conf.all.accept_redirects = 0
# net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.$eth.accept_redirects = 0
net.ipv4.conf.$eth.secure_redirects = 0
net.ipv4.conf.$eth.send_redirects = 0
EOF

for each in /proc/sys/net/ipv4/conf/*; do echo 0 > $each/accept_redirects; echo 0 > $each/send_redirects; echo 0 > $each/rp_filter; done

echo "for each in /proc/sys/net/ipv4/conf/*; do echo 0 > \$each/accept_redirects; echo 0 > \$each/send_redirects; echo 0 > \$each/rp_filter; done" >> /etc/rc.local

cat /etc/rc.local

sysctl -p

# cat >/usr/lib/firewalld/services/l2tpd.xml<<EOF
# <?xml version="1.0" encoding="utf-8"?>
# <service>
#   <short>l2tpd</short>
#   <description>L2TP IPSec</description>
#   <port protocol="udp" port="500"/>
#   <port protocol="udp" port="4500"/>
#   <port protocol="udp" port="1701"/>
# </service>
# EOF

# firewall-cmd --permanent --add-service=l2tpd
# firewall-cmd --permanent --add-service=ipsec
# firewall-cmd --permanent --add-masquerade
# firewall-cmd --reload

if [ -f /etc/csf/csf.conf ]; then

    # sed -i "s/TCP_IN = \"/TCP_IN = \"1701,/g" /etc/csf/csf.conf
    # sed -i "s/TCP_OUT = \"/TCP_OUT = \"1701,/g" /etc/csf/csf.conf
    # sed -i "s/TCP6_IN = \"/TCP6_IN = \"1701,/g" /etc/csf/csf.conf
    # sed -i "s/TCP6_OUT = \"/TCP6_OUT = \"1701,/g" /etc/csf/csf.conf    
    sed -i "s/UDP_IN = \"/UDP_IN = \"500,4500,1701,/g" /etc/csf/csf.conf
    sed -i "s/UDP_OUT = \"/UDP_OUT = \"500,4500,1701,/g" /etc/csf/csf.conf
    sed -i "s/UDP6_IN = \"/UDP6_IN = \"500,4500,1701,/g" /etc/csf/csf.conf
    sed -i "s/UDP6_OUT = \"/UDP6_OUT = \"500,4500,1701,/g" /etc/csf/csf.conf    

cat >/etc/csf/csfpre.sh<<EFF
#!/bin/bash
iptables -A FORWARD -d $iprange.0/24 -j ACCEPT
iptables -A FORWARD -s $iprange.0/24 -j ACCEPT
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT

iptables -t nat -A POSTROUTING -j MASQUERADE
iptables -t nat -A POSTROUTING -s $iprange.0/24 -o $eth -j MASQUERADE
iptables -t nat -A POSTROUTING -s $iprange.0/24 -j SNAT --to-source $serverip
EFF

cat /etc/csf/csfpre.sh
chmod 0700 /etc/csf/csfpre.sh

    csf -r

else
    iptables --table nat --append POSTROUTING --jump MASQUERADE
    iptables -t nat -A POSTROUTING -s $iprange.0/24 -o $eth -j MASQUERADE
    iptables -t nat -A POSTROUTING -s $iprange.0/24 -j SNAT --to-source $serverip
    service iptables save
fi

echo "nameserver 8.8.8.8" > /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf

systemctl enable ipsec xl2tpd
systemctl restart ipsec xl2tpd
# systemctl restart ipsec xl2tpd; csf -r; sleep 3; ipsec verify
clear

csf -r
sleep 5

ipsec verify

printf "
####################################################
#                                                  #
# This is a L2TP VPN installation for CentOS 7     #
# Version: 1.1.0 20140803                          #
# Original Author: Travis Lee                      #
# Website: http://www.stunnel.info                 #
# Modified for http://centminmod.com LEMP stacks   #
#                                                  #
####################################################
if there are no [FAILED] above, then you can
connect to your L2TP VPN Server with the default
user/password below:

ServerIP: $serverip
PSK: $mypsk

username: $username
password: $password
"

if [[ "$MULTIUSER" = y && "$MULTIUSERPASS" = y ]]; then

printf "
username: $usernameb
password: $passwordb

username: $usernamec
password: $passwordc

username: $usernamed
password: $passwordd

username: $usernamee
password: $passworde

username: $usernamef
password: $passwordf

username: $usernameg
password: $passwordg

username: $usernameh
password: $passwordh

"
fi